const cells = document.querySelectorAll(".cell");
const statusText = document.querySelector("#statusText");
const restartBtn = document.querySelector("#restartBtn");

const winConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

let scores = {
    X: 0,
    Draw: 0,
    O: 0
}

let loses = {
    X: 0,
    O: 0
}

let playing_mode = 0;
let options = ["", "", "", "", "", "", "", "", ""];
let player_1 = "X";
let player_2 = "O";
let currentPlayer = player_1;
let running = false;
let score = 0;

initializeGame();

function initializeGame(){
    setPlayer();
    setPlayingMode();
    cells.forEach(cell => cell.addEventListener("click", () => {
        const cellIndex = cell.getAttribute("cellIndex");

        if(options[cellIndex] != "" || !running){
            return;
        }

        updateCell(cell, cellIndex);
        checkWinner();
        if(playing_mode == 0) {
            computerTurn();
        }
    }));
    
    restartBtn.addEventListener("click", restartGame);
    statusText.textContent = `${currentPlayer}'s turn`;
    
    running = true;
}

function setPlayer() {
    var radioBtn = document.getElementsByName("option");
    let player;
    for (let i = 0; i < radioBtn.length; i++) {
        if(radioBtn[i].checked) {
            player = i;
        } 
    }

    currentPlayer = (player == 0) ? "X" : "O";
}

function setPlayingMode() {
    var radioBtn = document.getElementsByName("mode");
    for (let i = 0; i < radioBtn.length; i++) {
        if(radioBtn[i].checked) {
            playing_mode = i;
        } 
    }
}

function generateRandom(maxLimit){
    let rand = Math.random() * maxLimit;
    rand = Math.floor(rand); 
    return rand;
}

function computerTurn() {
    const turn = generateRandom(9);
    
    if(options[turn] != "" || !running){
        computerTurn();
        return;
    }

    updateCell(this, turn);
    cells.item(turn).textContent = currentPlayer;
    checkWinner();
}

function updateCell(cell, index){
    options[index] = currentPlayer;
    cell.textContent = currentPlayer;
}

function changePlayer(){
    currentPlayer = (currentPlayer == "X") ? "O" : "X";
    statusText.textContent = `${currentPlayer}'s turn`;
}

function checkWinner(){
    let roundWon = false;
    let winner = null;

    for(let i = 0; i < winConditions.length; i++){
        const condition = winConditions[i];

        const cellA = options[condition[0]];
        const cellB = options[condition[1]];
        const cellC = options[condition[2]];

        if(cellA == "" || cellB == "" || cellC == ""){
            continue;
        }

        if(cellA == cellB && cellB == cellC){
            roundWon = true;
            break;
        }
    }

    if(roundWon) { 
        var element1 = document.getElementById("won1");
        var element2 = document.getElementById("won2");

        var element3 = document.getElementById("lose1");
        var element4 = document.getElementById("lose2");
        winner = currentPlayer;
        scores[winner] +=1;

        if (winner == "X") {
            element1.textContent = scores[winner];
            loses["O"] +=1;
            element4.textContent = loses["O"];
        } else {
            element2.textContent = scores[winner];
            loses["X"] +=1;
            element3.textContent = loses["X"];
        }
        
        statusText.textContent = `${currentPlayer} wins!`;
        running = false;
        return winner;
    } else if(!options.includes("")) {
        var draw1 = document.getElementById("draw_1");
        var draw2 = document.getElementById("draw_2");
        scores["Draw"] += 1;
        draw1.textContent = scores["Draw"];
        draw2.textContent = scores["Draw"];
        statusText.textContent = `Draw!`;
        running = false;
        return "tie";
    } else {
        changePlayer();
    }
}

function restartGame(){
    setPlayer();
    setPlayingMode();
    options = ["", "", "", "", "", "", "", "", ""];

    statusText.textContent = `${currentPlayer}'s turn`;
    cells.forEach(cell => cell.textContent = "");
    running = true;
}
