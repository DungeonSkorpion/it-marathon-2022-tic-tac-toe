const express = require('express')
const urlencodedParser = express.urlencoded({extended: false});
const app = express()
const PORT = process.env.PORT || 80

app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

app.listen(PORT, () => {
    console.log("Server started on port " + PORT)
});